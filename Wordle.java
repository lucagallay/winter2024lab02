import java.util.Scanner;
import java.util.Random;

public class Wordle {

    public static String generateWord() {
        Random randomWord = new Random();
        String[] possibleWords = new String[]{"MISTY", "LYMPH", "OXIDE", "DEITY", "OCEAN", "AZURE", "CRIME", "WIDTH", "MELON", "NERDY", "VINYL", "PATCH",
                                        "TREND", "GNOME", "ARENA", "FLINT", "SCREW", "SIGMA", "DELTA", "VENOM"};

        int randomPosition = randomWord.nextInt(possibleWords.length); 
        
        return possibleWords[randomPosition]; // returns a word at a  pseudorandom index (0-20) to choose a word for the game
    }

    public static boolean letterInWord(String word, char letter) { // checks to see if a letter is in a word or not
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == letter) {
                return true;
            }
        }
        return false;
    }

    public static boolean letterInSlot(String word, char letter, int position) { // checks to see if a letter is in the right "slot" of the word
        
        // if a letter (char) isn't in the right slot (position) of the word (word), this method returns false
        if (position >= 0 && position < word.length()) {
            if (word.charAt(position) == letter) {
                return true;
            }
        } 
        return false;
    }

    public static String[] guessWord(String answer, String guess) { // creates an array of colours to be used to indicate the results of a user's guess through colour printing (see presentResults)
        String[] colours = new String[5];
        
        for (int i = 0; i < colours.length; i++) {
            char guessLetter = guess.charAt(i);

            if (letterInSlot(answer, guessLetter, i)) {
                colours[i] = "green"; // if letterInSlot goes through for all three parameters, it returns green
            } else if (letterInWord(answer, guessLetter)) {
                colours[i] = "yellow"; // if it doesn't go through for i, it means the letter isn't in the right slot, so it returns green
            } else {
                colours[i] = "white"; // the only other case would be that the letter is not in the word, so it returns white
            }
        }
        
        return colours; 
    }

    public static void presentResults(String word, String[] colours) { // this method makes the characters one of the three possible colours set in guessWord when printing to a terminal
        for (int i = 0; i < colours.length; i++) {
            if (colours[i].equals("green")) {
                System.out.print("\u001B[32m" + word.charAt(i) + "\u001B[0m"); 
            } else if (colours[i].equals("yellow")) {
                System.out.print("\u001B[33m" + word.charAt(i) + "\u001B[0m");
            } else if (colours[i].equals("white")) {
                System.out.print("\u001B[0m" + word.charAt(i) + "\u001B[0m");
            }
        }

        System.out.println();
    }

    public static String readGuess() {
        Scanner input = new Scanner(System.in); // user input section
        System.out.println("Input a 5-letter guess word: ");
        String guessInput = input.nextLine();

        while (guessInput.length() != 5) {
            System.out.println("Invalid guess. Please enter a 5-letter word."); // this game only takes 5-letter guesses
            System.out.println("Input a guess word: ");
            guessInput = input.nextLine();
        }
        return guessInput.toUpperCase(); // converts guess to uppercase
    }

    public static void runGame(String word) {

        int attemptsLeft = 6; // users get 6 attempts
        boolean gameWon = false; // controls the state of the game (active or stopped)

        while (attemptsLeft > 0 && !gameWon) {
            String guessInput = readGuess(); // references readGuess since the Scanner is defined there

            String[] colours = guessWord(word, guessInput);
            presentResults(guessInput, colours);

            attemptsLeft--; // attempt counter goes down by 1 after each guess

            if (attemptsLeft == 0 && !(guessInput.equals(word))) {
                System.out.println("Sorry! The word was " + word + ". Try again.");

            } else if (guessInput.equals(word)) {
                System.out.println("Nice! You win!");
                gameWon = true; // this ends the game
                
            }
        }



    }
}
