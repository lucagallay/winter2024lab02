import java.util.Scanner; 

public class gameLauncher {
    public static void main (String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Hello! Enter '1' for Hangman or '2' for Wordle");

        int gameSelect = userInput.nextInt();
        userInput.nextLine();

        Scanner hangmanInput = new Scanner(System.in);
        
        if (gameSelect == 1){
                System.out.println("Enter a four-lettered word: "); 
                Hangman.runGame(hangmanInput.nextLine());     
        }

        if (gameSelect == 2){
                Wordle.runGame(Wordle.generateWord());
        }

        userInput.close();
        hangmanInput.close();
    
    }
}



