import java.util.Scanner;  

public class Hangman {
    public static void main(String []args) {
        /* Scanner input = new Scanner(System.in);

        System.out.println("Enter a four-lettered word: ");
        String word = input.nextLine();
        
        runGame(word);

        input.close(); */
    }

    public static int isLetterInWord(String word, char c) {
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == c) {
                return i;
            }
        }
        return -1;
    }

    public static char toUpperCase(char c) {
        return Character.toUpperCase(c);
    }

    public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
        System.out.print("Your result is ");

       if (letter0 == true) {
            System.out.print(word.charAt(0));
        } else {  
            System.out.print("_");
        }
    
        if (letter1 == true) {
            System.out.print(word.charAt(1));
        } else {  
            System.out.print("_");
        }
    
        if (letter2 == true) {
            System.out.print(word.charAt(2));
        } else {
            System.out.print("_");
        }
    
    
        if (letter3 == true) {
            System.out.println(word.charAt(3));
  
        } else {
            System.out.println("_");
            
        }
    }

    public static void runGame(String word) {
        int triesLeft = 6;
        
        boolean letter0 = false;
        boolean letter1 = false;
        boolean letter2 = false;
        boolean letter3 = false;

        Scanner input = new Scanner(System.in);
        
        while (triesLeft > 0) {
            System.out.println("Guess a letter: ");

            char c = toUpperCase(input.next().charAt(0));

            int charPosition = isLetterInWord(word, c);

            if (charPosition >= 0) {

                if (charPosition == 0) {
                    letter0 = true;
                } 

                if (charPosition == 1) {
                    letter1 = true;
                }

                if (charPosition == 2) {
                    letter2 = true;
                } 

                if (charPosition == 3) {
                    letter3 = true;
                }

                if (letter0 == true && letter1 && letter2 && letter3) {
                    triesLeft = 0;
                    System.out.println("You got it!");
                }
              
            }
         
            else {
                triesLeft -= 1;
                System.out.println("Nope, sorry. You have " + triesLeft +" tries left. ");
                
                if (triesLeft == 0) {
                    System.out.println("You lost.");
                }
            }
            
            printWork(word, letter0, letter1, letter2, letter3);
          
        }

        input.close();
    }
 }