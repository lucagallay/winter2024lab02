import java.util.Scanner;

public class Driver {
    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Hello! Welcome to the calculator!");
        
        // add
        System.out.println("Input two numbers to add:");

        int a = input.nextInt();
        int b = input.nextInt();
    
        System.out.println("That adds up to " + Calculator.addNum(a, b));

        // sqrt
        System.out.println("Input a number to find its square root: ");
        int c = input.nextInt();
        System.out.println(Calculator.sqrtNum(c));

        // random number
        System.out.println("Just for all of your hard work, here's a random number: " + Calculator.randoNum());

        input.close();
    }
}